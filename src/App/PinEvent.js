import { FreeCameraOptions } from "mapbox-gl";
import app from "./App.js";


export class PinEvent {

    title;
    desc;
    dateStart;
    dateEnd;
    lon;
    lat;

    domContainer;
    domTitle;
    domDesc;
    domTime;

    constructor( jsonPin ) {
        this.title = jsonPin.title;
        this.desc = jsonPin.desc;
        this.dateStart = jsonPin.dateStart;
        this.dateEnd = jsonPin.dateEnd;
        this.lon = jsonPin.lon;
        this.lat = jsonPin.lat;


        this.domContainer = document.createElement('div');
        this.domContainer.innerHTML = `
            <div class="marker-popup-wrapper">
                <h1 class="title"></h1>
                <div class="time"></div>
                <p class="desc"></p>
            </div>
        `;
        // this.domContainer.addEventListener( 'click', this.onPostItClick.bind(this) );

        this.domTitle = this.domContainer.querySelector( '.title' );
        this.domTitle.textContent = this.title;

        this.domDesc = this.domContainer.querySelector( '.desc' );
        this.domDesc.textContent = this.desc;

        this.domTime = this.domContainer.querySelector( '.time' );

        const optionsDate = {
            weekday: "long",
            year: "numeric",
            month: "long",
            day: "numeric",
            hour: "2-digit",
            minute: "2-digit",
            hc: "h24"
        };

        this.domTime.innerHTML = `
            <div class="event-start">Débute le: ${new Date(this.dateStart).toLocaleDateString('fr-FR', optionsDate)}</div>
            <div class="event-end">Fini le: ${new Date(this.dateEnd).toLocaleDateString('fr-FR', optionsDate)}</div>
        `;

        console.log(this.domContainer);
        // const actualTime = this.updatedAt;
        // const date = new Date( actualTime );
        // this.domTime.textContent = date.toString();
    }

    // Appelée automatiquement par JSON.stringify()
    toJSON() {
        return {
            title: this.title,
            desc: this.desc,
            dateStart: this.dateStart,
            dateEnd: this.dateEnd,
            lon: this.lon,
            lat: this.lat
        }
    }

}