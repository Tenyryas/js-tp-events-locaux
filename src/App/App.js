import '../index.html';
// le chemin relatif pointe déjà sur le dossier node_modules
import 'mapbox-gl/dist/mapbox-gl.css';
import '../style.css';
import appConfig from '../../app.config.js';
import { PinEvent } from './PinEvent.js';


const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

class App {

    domPinTitle;
    domPinDesc;
    domPinDateStart;
    domPinDateEnd;
    domPinLon;
    domPinLat;
    domBtnNewAdd;

    
    map;
    pins = [];

    start() {

        let that = this;
        console.log('Application started');
        this.initHtml();

        mapboxgl.accessToken = appConfig.accessToken;
        this.map = new mapboxgl.Map({
            container: 'map',
            center: { lng: 2.7885292, lat: 42.6832109 },
            zoom: 14,
            maxPitch: 0,
            dragRotate: false,
            customAttribution: 'Mon super site de la mort',
            // maxBounds: [[2.7625387, 42.6874962], [2.8178099, 42.6774791]],
            style: 'mapbox://styles/mapbox/streets-v11'
        });

        // Ajout d'un bouton controle du zoom
        const zoomCtrl = new mapboxgl.NavigationControl();
        this.map.addControl(zoomCtrl);


        // Ajout d'un bouton de refresh
        const refresh = document.getElementById('refresh');
        refresh.addEventListener('click', function(e) {
            location.reload();
        });

        // Ajout d'un écouteur de clic sur la map (fonctionne pareil que addEventListener())
        this.map.on('click', function (evt) {
            that.domPinLon.value = evt.lngLat.lng;
            that.domPinLat.value = evt.lngLat.lat;
        });

        this.loadPins();
        this.renderPins();
        //42.682661, 2.793097
    }



    initHtml() {
        this.domPinTitle = document.querySelector('#eventName');
        this.domPinDesc = document.querySelector('#eventDesc');

        this.domPinDateStart = document.querySelector('#startEventDate');
        this.domPinDateEnd = document.querySelector('#endEventDate');

        this.domPinLon = document.querySelector('#longCoorEvent');
        this.domPinLat = document.querySelector('#latCoorEvent');

        this.domBtnNewAdd = document.querySelector('#submit');
        this.domBtnNewAdd.addEventListener('click', this.onBtnNewAddClick.bind(this));
    }



    loadPins() {
        const storageContent = localStorage.getItem(appConfig.localStorageName);
        if (storageContent === null) {
            return;
        }

        let storedJson;

        try {
            storedJson = JSON.parse(storageContent);
        }
        // Si le contenu est corrompu, on le supprime et on arrête de traitement
        catch (error) {
            localStorage.removeItem(appConfig.localStorageName);
            return;
        }

        // On crée les postIts à partir du JSON obtenu
        for (let jsonPin of storedJson) {
            const pinEvent = new PinEvent(jsonPin);
            this.pins.push(pinEvent);
        }
    }



    renderPins() {
        // 1- Suppression de la la liste actuelle de l'affichage
        // this.domList.innerHTML = '';

        // 2- Reconstruction à partir de la liste en mémoire
        for (let pin of this.pins) {

            // 3 jours en ms
            const threeDays = (3600 * 24 * 1000) * 3;
            let markerColor;
            
            // #bf2d00 rouge
            // #ff7f00 orange
            // #3ec200 vert
            
            // Vérifier si l'event commence dans 3 jours ou moins, sinon commence dans + de 3 jours
            if( ( Date.parse(pin.dateStart) - Date.now() ) <= threeDays ) {
                markerColor = '#ff7f00';
            }
            else {
                markerColor = '#3ec200';
            }
            // Vérifier que l'event est toujours en cours
            if( ( Date.parse(pin.dateEnd) - Date.now() ) <= 0 ) {
                markerColor = '#bf2d00';
            }

            const marker = new mapboxgl.Marker({ color: markerColor });
            const popup = new mapboxgl.Popup();
            marker.setLngLat( {lng: pin.lon, lat: pin.lat} );
            marker.getElement().title = pin.title;
            popup.setHTML(pin.domContainer.innerHTML);
            marker.setPopup( popup );
            console.log(marker);
            marker.addTo(this.map);
        }  
    }


    onBtnNewAddClick() {
        // Grâce au bind() sur l'écouteur, "this" représente l'instance de App
        // (à la place de l'élément cliqué)
        // console.log(this);

        // Etapes d'ajout
        // On récupère et on vérifie les données saisies
        let hasError = false;
        const newTitle = this.domPinTitle.value.trim();
        if (newTitle === '') {
            this.domPinTitle.classList.add('error');
            this.domPinTitle.value = '';
            hasError = true;
        }

        const newDesc = this.domPinDesc.value.trim();
        if (newDesc === '') {
            this.domPinDesc.classList.add('error');
            this.domPinDesc.value = '';
            hasError = true;
        }

        const newDateStart = this.domPinDateStart.value.trim();
        if (newDateStart === '') {
            this.domPinDateStart.classList.add('error');
            this.domPinDateStart.value = '';
            hasError = true;
        }

        const newDateEnd = this.domPinDateEnd.value.trim();
        if (newDateEnd === '') {
            this.domPinDateEnd.classList.add('error');
            this.domPinDateEnd.value = '';
            hasError = true;
        }

        const newLon = this.domPinLon.value.trim();
        if (newLon === '') {
            this.domPinLon.classList.add('error');
            this.domPinLon.value = '';
            hasError = true;
        }

        const newLat = this.domPinLat.value.trim();
        if (newLat === '') {
            this.domPinLat.classList.add('error');
            this.domPinLat.value = '';
            hasError = true;
        }

        // Si on a une ou plusieurs erreurs, on arrête le traitement
        if (hasError) {
            return;
        }



        // Création d'une version JSON du Pin, puis vidage des champs
        const jsonPin = {
            title: newTitle,
            desc: newDesc,
            dateStart: newDateStart,
            dateEnd: newDateEnd,
            lon: newLon,
            lat: newLat
        };
        this.domPinTitle.value = this.domPinDesc.value = this.domPinDateStart.value = this.domPinDateEnd.value = '';

        // Fabriquer le Pin
        const newMarker = new PinEvent(jsonPin);


        // Enregistrement dans l'application (RAM)
        this.pins.push(newMarker);
        // Enregistrement dans localStorage
        localStorage.setItem(appConfig.localStorageName, JSON.stringify(this.pins));

        this.renderPins();
    }
}

const instance = new App();

export default instance;